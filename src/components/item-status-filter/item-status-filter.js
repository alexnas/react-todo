import React, { Component } from 'react';
import './item-status-filter.css';

export default class ItemStatusFilter extends Component {
  buttons = [
    { name: 'all', label: 'All' },
    { name: 'active', label: 'Active' },
    { name: 'done', label: 'Done' }
  ];

  render() {
    const { filter, onClickFilter } = this.props;

    const button = this.buttons.map(({ name, label }) => {
      const isActive = name === filter;
      const clazz = isActive ? 'btn-info' : 'btn-outline-secondary';
      const className = 'btn ' + clazz;

      return (
        <button
          type='button'
          className={className}
          key={name}
          name={name}
          onClick={() => onClickFilter(name)}
        >
          {label}
        </button>
      );
    });

    return <div className='btn-group item-status-filter'>{button}</div>;
  }
}
