import React, { Component } from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import ItemAddForm from '../item-add-form';

import './app.css';

export default class App extends Component {
  maxId = 100;

  state = {
    todoData: [
      this.createTodoItem('Drink Coffee'),
      this.createTodoItem('Make Awesome App'),
      this.createTodoItem('Have a lunch')
    ],
    term: '',
    filter: 'active' // active, all, done
  };

  deleteItem = id => {
    this.setState(({ todoData }) => {
      const idx = todoData.findIndex(el => el.id === id);
      const newArray = [...todoData.slice(0, idx), ...todoData.slice(idx + 1)];

      return {
        todoData: newArray
      };
    });
  };

  createTodoItem(label) {
    return {
      label,
      done: false,
      important: false,
      id: this.maxId++
    };
  }

  addItem = text => {
    const newItem = this.createTodoItem(text);
    this.setState(({ todoData }) => {
      const newArr = [...todoData, newItem];
      return { todoData: newArr };
    });
  };

  toggleProperty(arr, id, propName) {
    const idx = arr.findIndex(el => el.id === id);
    const oldItem = arr[idx];
    const newItem = { ...oldItem, [propName]: !oldItem[propName] };
    return [...arr.slice(0, idx), newItem, ...arr.slice(idx + 1)];
  }

  onToggleImportant = id => {
    this.setState(({ todoData }) => {
      return { todoData: this.toggleProperty(todoData, id, 'important') };
    });
  };

  onToggleDone = id => {
    this.setState(({ todoData }) => {
      return { todoData: this.toggleProperty(todoData, id, 'done') };
    });
  };

  onSearchTodos(todos, term) {
    return todos.filter(
      el =>
        term === '' || el.label.toLowerCase().indexOf(term.toLowerCase()) > -1
    );
  }

  onChangeSearch = text => {
    this.setState({ term: text });
  };

  onFilter____(todos, filter) {
    if (filter === 'all') {
      return todos;
    }
    if (filter === 'done') {
      return todos.filter(el => el.done);
    }
    if (filter === 'active') {
      return todos.filter(el => !el.done);
    }
    return todos;
  }

  onFilter(todos, filter) {
    switch (filter) {
      case 'all':
        return todos;
      case 'done':
        return todos.filter(el => el.done);
      case 'active':
        return todos.filter(el => !el.done);
      default:
        return todos;
    }
  }

  onClickedFilter = name => {
    this.setState({ filter: name });
  };

  render() {
    const { todoData, term, filter } = this.state;
    const doneCount = todoData.filter(el => el.done).length;
    const restCount = todoData.length - doneCount;
    const visibleItems = this.onFilter(
      this.onSearchTodos(todoData, term),
      filter
    );

    return (
      <div className='todo-app'>
        <AppHeader toDo={restCount} done={doneCount} />
        <div className='top-panel d-flex'>
          <SearchPanel onChangeSearch={this.onChangeSearch} term={term} />
          <ItemStatusFilter
            filter={filter}
            onClickFilter={this.onClickedFilter}
          />
        </div>

        <TodoList
          todos={visibleItems}
          onDeleted={this.deleteItem}
          onToggleImportant={this.onToggleImportant}
          onToggleDone={this.onToggleDone}
          done={this.state.done}
          important={this.state.important}
        />

        <ItemAddForm onItemAdded={this.addItem} />
      </div>
    );
  }
}
