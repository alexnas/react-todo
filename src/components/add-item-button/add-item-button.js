import React from 'react';

const AddItemButton = ({ onAdded }) => {

    return (
        <div>
            <button type="button"
                className="btn btn-outline-success float-right"
                onClick={onAdded}
            >
                {/* <i className="fa fa-exclamation" /> */}
                Add Item
            </button>

        </div>
    );

}

export default AddItemButton;