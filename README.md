This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# React ToDo

This simple React Todo application designed as example how to organize (create, search, mark and delete) a list of current affairs.

## Uses

- React 16.8
- Initial data set is hardcoded in app.js

## Installation and Usage

- Be sure that Git and Node.js are installed globally.
- Clone this repo.
- Run `npm install`, all required components will be installed automatically.
- Run `npm start` to start the project.
- Run `npm build` to create a build directory with a production build of the app.
- Run `npm test` to test the app.

## License

Under the terms of the MIT license.
